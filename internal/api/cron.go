package api

import (
	"github.com/robfig/cron/v3"
)

func (h *Handler) InitCron() *cron.Cron {
	// Init cron api
	crontab := cron.New()

	// Init processor
	_, _ = crontab.AddFunc("@every 10s", func() { h.Oauth.deleteExpiredTokens() })

	return crontab
}
