package api

import (
	"github.com/go-playground/validator/v10"
	"github.com/gofiber/fiber/v2"

	"gitlab.com/bugielektrik/phobos/oauth-service/internal/model"
)

type OauthService interface {
	AuthorizeAccount(oauth *model.Oauth) (*model.Oauth, error)
	CheckToken(accessToken string) (*model.Oauth, error)
	DeleteExpiredTokens()
}

type Oauth struct {
	service OauthService
}

func NewOauthHandler(s OauthService) *Oauth {
	return &Oauth{service: s}
}

func (h *Oauth) authorizeAccount(c *fiber.Ctx) error {
	// Bind params
	oauthDest := new(model.Oauth)
	if err := c.BodyParser(oauthDest); err != nil {
		c.Status(fiber.StatusBadRequest)
		return c.JSON(fiber.Map{"error": err.Error()})
	}

	// Validate params
	if err := validator.New().Struct(oauthDest); err != nil {
		c.Status(fiber.StatusBadRequest)
		return c.JSON(fiber.Map{"error": err.Error()})
	}

	// Write data to DB
	oauthSrc, err := h.service.AuthorizeAccount(oauthDest)
	if err != nil {
		c.Status(fiber.StatusInternalServerError)
		return c.JSON(fiber.Map{"error": err.Error()})
	}

	return c.JSON(oauthSrc)
}

func (h *Oauth) checkToken(c *fiber.Ctx) error {
	// Bind params
	accessToken := c.Query("access_token")

	// Read data from DB
	oauthSrc, err := h.service.CheckToken(accessToken)
	if err != nil {
		c.Status(fiber.StatusInternalServerError)
		return c.JSON(fiber.Map{"error": err.Error()})
	}

	if oauthSrc == nil {
		c.Status(fiber.StatusNotFound)
	}

	oauthSrc.IsActive = true
	return c.JSON(oauthSrc)
}

func (h *Oauth) deleteExpiredTokens() {
	h.service.DeleteExpiredTokens()
}
