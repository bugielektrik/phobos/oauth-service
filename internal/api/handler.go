package api

type Dependencies struct {
	OauthService OauthService
}

type Handler struct {
	Oauth *Oauth
}

func New(d Dependencies) *Handler {
	return &Handler{
		Oauth: NewOauthHandler(d.OauthService),
	}
}
