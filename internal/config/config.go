package config

import (
	"net/url"
	"os"
	"time"

	"github.com/gofiber/fiber/v2/middleware/csrf"
	"github.com/gofiber/fiber/v2/middleware/limiter"
	"github.com/spf13/viper"
)

const (
	defaultHTTPPort               = "9001"
	defaultHTTPRWTimeout          = 10 * time.Second
	defaultHTTPMaxHeaderMegabytes = 1
	EnvLocal                      = "local"
)

type (
	Config struct {
		Environment string
		HTTP        HTTPConfig
		Csrf        csrf.Config
		Limiter     limiter.Config
		Postgres    PostgresConfig
		Oauth       OauthConfig
	}

	HTTPConfig struct {
		Port               string        `mapstructure:"port"`
		ReadTimeout        time.Duration `mapstructure:"readTimeout"`
		WriteTimeout       time.Duration `mapstructure:"writeTimeout"`
		MaxHeaderMegabytes int           `mapstructure:"maxHeaderBytes"`
	}

	PostgresConfig struct {
		DataSourceName string `mapstructure:"dataSourceName"`
	}

	OauthConfig struct {
		ExpiresIn string `mapstructure:"expiresIn"`
		TokenType string `mapstructure:"tokenType"`
	}
)

func Init(configsDir, project string) (*Config, error) {
	populateDefaults()

	if err := parseConfigFile(configsDir, os.Getenv("ENV")); err != nil {
		return nil, err
	}

	var cfg Config
	if err := unmarshal(&cfg); err != nil {
		return nil, err
	}

	setFromEnv(&cfg)

	source, err := url.Parse(cfg.Postgres.DataSourceName)
	if err != nil {
		return nil, err
	}
	sourceQuery := source.Query()
	sourceQuery.Set("search_path", project)
	source.RawQuery = sourceQuery.Encode()
	cfg.Postgres.DataSourceName = source.String()

	return &cfg, nil
}

func unmarshal(cfg *Config) error {
	if err := viper.UnmarshalKey("postgres", &cfg.Postgres); err != nil {
		return err
	}

	if err := viper.UnmarshalKey("oauth", &cfg.Oauth); err != nil {
		return err
	}

	return viper.UnmarshalKey("http", &cfg.HTTP)
}

func setFromEnv(cfg *Config) {
	cfg.HTTP.Port = os.Getenv("PORT")

	cfg.Environment = os.Getenv("ENV")

	cfg.Postgres.DataSourceName = os.Getenv("DATABASE_URL")
}

func parseConfigFile(folder, env string) error {
	viper.AddConfigPath(folder)
	viper.SetConfigName("main")

	if err := viper.ReadInConfig(); err != nil {
		return err
	}

	if env == EnvLocal {
		return nil
	}

	viper.SetConfigName(env)

	return viper.MergeInConfig()
}

func populateDefaults() {
	viper.SetDefault("http.port", defaultHTTPPort)
	viper.SetDefault("http.max_header_megabytes", defaultHTTPMaxHeaderMegabytes)
	viper.SetDefault("http.timeouts.read", defaultHTTPRWTimeout)
	viper.SetDefault("http.timeouts.write", defaultHTTPRWTimeout)
}
