package model

type Oauth struct {
	IsActive    bool   `json:"active"`
	InternalID  string `json:"-" db:"id"`
	AccessToken string `json:"access_token,omitempty" db:"access_token"`
	ExpiresIn   string `json:"expires_in,omitempty" db:"expires_in"`
	TokenType   string `json:"token_type,omitempty" db:"token_type"`
	AccountID   string `json:"account_id,omitempty" db:"account_id" validate:"required"`
	AccountType int    `json:"account_type,omitempty" db:"account_type" validate:"required"`
	CreatedAt   string `json:"created_at,omitempty" db:"created_at"`
	UpdatedAt   string `json:"updated_at,omitempty" db:"updated_at"`
}
