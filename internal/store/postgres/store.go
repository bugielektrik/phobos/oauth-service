package postgres

import (
	"github.com/hashicorp/go-hclog"
	"github.com/jmoiron/sqlx"
)

type Store struct {
	Oauth *Oauth
}

func New(db *sqlx.DB, logger hclog.Logger) *Store {
	return &Store{
		Oauth: NewOauthStore(db, logger),
	}
}
