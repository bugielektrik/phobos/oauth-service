package postgres

import (
	"context"
	"time"

	"github.com/hashicorp/go-hclog"
	"github.com/jmoiron/sqlx"

	"gitlab.com/bugielektrik/phobos/oauth-service/internal/model"
)

type Oauth struct {
	db     *sqlx.DB
	logger hclog.Logger
}

func NewOauthStore(db *sqlx.DB, logger hclog.Logger) *Oauth {
	return &Oauth{
		db:     db,
		logger: logger,
	}
}

func (s *Oauth) Create(data *model.Oauth) error {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	_, err := s.db.NamedExecContext(ctx, `
		INSERT INTO oauths (account_id, account_type, access_token, expires_in, token_type) 
		VALUES(:account_id, :account_type, :access_token, :expires_in, :token_type)`,
		data)
	return err
}

func (s *Oauth) Get(accessToken string) (*model.Oauth, error) {
	query := `
		SELECT account_id
		FROM oauths
		WHERE access_token=:access_token`
	args := map[string]interface{}{
		"access_token": accessToken,
	}
	data, err := s.getRow("Get", query, args)
	if err != nil {
		return nil, err
	}
	return data, nil
}

func (s *Oauth) GetByID(accessToken, tokenExpires string) (*model.Oauth, error) {
	query := `
		SELECT *
		FROM oauths
		WHERE access_token=:access_token AND created_at > (CURRENT_TIMESTAMP - INTERVAL '` + tokenExpires + ` seconds')`
	args := map[string]interface{}{
		"access_token": accessToken,
	}
	data, err := s.getRow("GetByID", query, args)
	if err != nil {
		return nil, err
	}
	return data, nil
}

func (s *Oauth) DeleteExpiredTokens(tokenExpires string) {
	logger := s.logger.With("operation", "DeleteExpiredTokens")

	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	_, err := s.db.NamedExecContext(ctx, `
		DELETE FROM oauths
		WHERE id IN (
		    SELECT id
		    FROM oauths
		    WHERE created_at < (CURRENT_TIMESTAMP - INTERVAL '`+tokenExpires+` seconds')
		)`,
		map[string]interface{}{})
	if err != nil {
		logger.Error("failed to get", "error", err)
	}
}

func (s *Oauth) getRow(operation, query string, args interface{}) (*model.Oauth, error) {
	logger := s.logger.With("operation", operation)

	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	row, err := s.db.NamedQueryContext(ctx, query, args)
	if err != nil {
		logger.Error("failed to get", "error", err)
		return nil, err
	}
	defer row.Close()

	if !row.Next() {
		logger.Debug("not found")
		return nil, nil
	}

	data := new(model.Oauth)
	err = row.StructScan(&data)
	if err != nil {
		logger.Error("failed to scan into struct", "error", err)
		return nil, err
	}

	return data, nil
}
