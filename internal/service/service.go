package service

type Dependencies struct {
	ExpiresIn  string
	TokenType  string
	OauthStore OauthStore
}

type Service struct {
	Oauth *Oauth
}

func New(d Dependencies) *Service {
	return &Service{
		Oauth: NewOauthService(d.ExpiresIn, d.TokenType, d.OauthStore),
	}
}
