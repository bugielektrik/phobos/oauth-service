package service

import (
	"github.com/google/uuid"

	"gitlab.com/bugielektrik/phobos/oauth-service/internal/model"
)

type OauthStore interface {
	Create(data *model.Oauth) error
	Get(accessToken string) (*model.Oauth, error)
	GetByID(accessToken, tokenExpires string) (*model.Oauth, error)
	DeleteExpiredTokens(tokenExpires string)
}

type Oauth struct {
	expiresIn string
	tokenType string
	store     OauthStore
}

func NewOauthService(expiresIn, tokenType string, s OauthStore) *Oauth {
	return &Oauth{
		expiresIn: expiresIn,
		tokenType: tokenType,
		store:     s,
	}
}

func (s *Oauth) AuthorizeAccount(oauthDest *model.Oauth) (*model.Oauth, error) {
	// Generate data AccessToken
	for {
		accessToken := uuid.New().String()

		oauthSrc, err := s.store.Get(accessToken)
		if err != nil {
			return nil, err
		}

		if oauthSrc == nil {
			oauthDest.AccessToken = accessToken
			break
		}
	}

	oauthDest.ExpiresIn = s.expiresIn
	oauthDest.TokenType = s.tokenType

	err := s.store.Create(oauthDest)
	return oauthDest, err
}

func (s *Oauth) CheckToken(accessToken string) (*model.Oauth, error) {
	oauthSrc, err := s.store.GetByID(accessToken, s.expiresIn)
	return oauthSrc, err
}

func (s *Oauth) DeleteExpiredTokens() {
	s.store.DeleteExpiredTokens(s.expiresIn)
}
