FROM golang:1.18-alpine AS builder
WORKDIR /source
COPY . /source
RUN CGO_ENABLED=0 GOOS=linux go build -mod vendor -o oauth-service ./cmd/.

FROM alpine:3.15
RUN mkdir /app
WORKDIR /app
COPY --from=builder /source/oauth-service /usr/local/bin
COPY --from=builder /source/configs /app/configs
COPY --from=builder /source/migrations /app/migrations
RUN chmod a+x /usr/local/bin/oauth-service
ENTRYPOINT [ "oauth-service" ]
