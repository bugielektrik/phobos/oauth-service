CREATE TABLE IF NOT EXISTS oauths (
    id              SERIAL PRIMARY KEY,
    created_at      TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updated_at      TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    access_token    VARCHAR NOT NULL UNIQUE,
    expires_in      VARCHAR NOT NULL,
    token_type      VARCHAR NOT NULL,
    account_id      VARCHAR NOT NULL,
    account_type    VARCHAR NOT NULL
);